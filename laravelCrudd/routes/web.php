<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     //return view('welcome');

// });
Route::any('/', 'UserController@index');
Route::any('/dashboard', 'UserController@dashboard');
Route::any('/logout', 'UserController@logout');
Route::any('/addchoice', 'UserController@addchoice');
Route::any('/getchoice', 'UserController@getchoice');
Route::any('/deletechoice', 'UserController@deletechoice');
//Route::match(['get', 'post'], '/', 'UserController@index');


<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Auth;

class UserController extends Controller{


    public function index(Request $request){
    	if ($_POST) {
	    	$users = DB::table('users')->select('*')->get()->first();
	    	if ($_POST['username'] == $users->user_email && $_POST['password'] == $users->user_pw) {
	    		$user_id = $users->id;

	    		Auth::loginUsingId($user_id);
	    		return redirect('/dashboard');
	    	}else{
    			return view('login');
	    	}
    	}else{
    		if (Auth::check()) {
    			return redirect('dashboard');
	    	}else{
	    		return view('login');
	    	}
    	}
    }

    public function dashboard(){
    	if (Auth::check()) {
    		return view('dashboard');
    	}else{
    		return redirect('/');
    	}
    }

   	public function logout($value=''){
   		if (Auth::check()) {
    		Auth::logout();
	   		return redirect('/');
    	}else{
    		return redirect('/');
    	}
   	}

    public function addchoice(Request $request){
        $insert = DB::table('choice')->insert($request->all());
        if ($insert){
            return response()->json([
                'status' => true,
                'message' => 'Choice Added Successfully']);
        }
    }

    public function getchoice(){
        $result = DB::table('choice')->select()->get();
        return response()->json([
                'status' => true,
                'message' => 'Choice Added Successfully',
                'data' => $result]); 
    }
    //Index Page Delete the Data
    public function deletechoice(Request $request){
        $data = $request->all();
        $delete = DB::table('choice')->where('id', $data[0])->delete();
        if ($delete){
            return response()->json([
                'status' => true,
                'message' => 'Choice Deleted Successfully']);
        }
    }
}

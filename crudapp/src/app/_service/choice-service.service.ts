import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChoiceServiceService {

  constructor(private http: HttpClient) { }

  	addChoice(postData:any){
  		return this.http.post(`/api/addchoice`, postData)
  		.pipe(
  			map((res: any)=>{
  				return res;
  			}),
  		)
  	}

  	getChoice(){
  		return this.http.get(`/api/getchoice`)
  		.pipe(
  			map((res: any)=>{
  				return res;
  			}),
  		)
  	}

  	deleteChoice(postData:any){
  		return this.http.post(`/api/deletechoice`, postData)
  		.pipe(
  			map((res: any)=>{
  				return res;
  			}),
  		)
  	}
}

import { TestBed } from '@angular/core/testing';

import { ChoiceServiceService } from './choice-service.service';

describe('ChoiceServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChoiceServiceService = TestBed.get(ChoiceServiceService);
    expect(service).toBeTruthy();
  });
});

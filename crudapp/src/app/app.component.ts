import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ChoiceServiceService} from './_service/choice-service.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  	title = 'crudapp';
  	crudForm: FormGroup;
  	submitted = false;

    choice:any;

    constructor(private fb: FormBuilder, private choiceService: ChoiceServiceService) { }

    ngOnInit() {
	    this.crudForm = this.fb.group({
	    	food_name:['', Validators.required],
	    	food_color:['', Validators.required]
	    })
      this.getChoice();
    }
    get f() { return this.crudForm.controls; }

    onSubmit(){
    	this.submitted = true;
    	if (this.crudForm.invalid) return;
    	this.choiceService.addChoice(this.crudForm.value).
      subscribe((res:any) =>{
          this.getChoice();
          alert(res.message);
          location.reload();
      })
    }

    getChoice(){
        this.choiceService.getChoice().
        subscribe((res:any) =>{
            this.choice = res.data;
        })
    }

    deleteChoice($id){
        this.choiceService.deleteChoice($id).
        subscribe((res:any) =>{
            this.getChoice();
            alert(res.message);
            location.reload();
        })
    }
}
